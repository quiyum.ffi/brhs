<?php
require_once("../../vendor/autoload.php");
require_once("../templateLayout/templateInformation.php");
use App\Authentication;
use App\Utility\Utility;
if($_SESSION['role_status']==0){
    $auth= new Authentication();
    $status = $auth->setData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('../user/login.php');
        return;
    }
}
else {
    Utility::redirect('../user/login.php');
}
?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title;?></title>
    <?php include ("../templateLayout/css/templateCss.php");?>
</head>
<body>
<div class="fh5co-loader"></div>
<div id="page">
    <?php include ("../templateLayout/adminNavigation.php");?>
<div id="fh5co-contact" class="fh5co-no-pd-top">
    <div class="container">
        <div class="row animate-box">
            <div class="col-md-12 col-md-offset-0 text-center fh5co-heading">
                <h2><span>Manual Registration</span></h2>
            </div>
        </div>

                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <form action="../../controller/processManualReg.php" method="post" enctype="multipart/form-data">
                            <h3 style="color:red">[Note: Registration fee 1000/- (per person)| For child 800/- (per child)]</h3>
                            <div class="form-group row">
                                <div class="col-md-12 field">
                                    <label>Name</label>
                                    <input type="text" name="name" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12 field">
                                    <label>Father/Husband's Name</label>
                                    <input type="text" name="f_h_name" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12 field">
                                    <label>Mother's Name</label>
                                    <input type="text" name="m_name" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12 field">
                                    <label>Gender</label>
                                    <label class="radio-inline"><input type="radio" name="gender" value="Male" required>Male</label>
                                    <label class="radio-inline"><input type="radio" name="gender" value="Female" required>Female</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12 field">
                                    <label>Passing Year</label>
                                    <select class="form-control" name="passing_year" required>
                                        <option value="2017">2017</option>
                                        <option value="2016">2016</option>
                                        <option value="2015">2015</option>
                                        <option value="2014">2014</option>
                                        <option value="2013">2013</option>
                                        <option value="2012">2012</option>
                                        <option value="2011">2011</option>
                                        <option value="2010">2010</option>
                                        <option value="2009">2009</option>
                                        <option value="2008">2008</option>
                                        <option value="2007">2007</option>
                                        <option value="2006">2006</option>
                                        <option value="2005">2005</option>
                                        <option value="2004">2004</option>
                                        <option value="2003">2003</option>
                                        <option value="2002">2002</option>
                                        <option value="2001">2001</option>
                                        <option value="2000">2000</option>
                                        <option value="1999">1999</option>
                                        <option value="1998">1998</option>
                                        <option value="1997">1997</option>
                                        <option value="1996">1996</option>
                                        <option value="1995">1995</option>
                                        <option value="1994">1994</option>
                                        <option value="1993">1993</option>
                                        <option value="1992">1992</option>
                                        <option value="1991">1991</option>
                                        <option value="1990">1990</option>
                                        <option value="1989">1989</option>
                                        <option value="1988">1988</option>
                                        <option value="1987">1987</option>
                                        <option value="1986">1986</option>
                                        <option value="1985">1985</option>
                                        <option value="1984">1984</option>
                                        <option value="1983">1983</option>
                                        <option value="1982">1982</option>
                                        <option value="1981">1981</option>
                                        <option value="1980">1980</option>
                                        <option value="1979">1979</option>
                                        <option value="1978">1978</option>
                                        <option value="1977">1977</option>
                                        <option value="1976">1976</option>
                                        <option value="1975">1975</option>
                                        <option value="1974">1974</option>
                                        <option value="1973">1973</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12 field">
                                    <label>Profession</label>
                                    <input type="text" name="profession" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12 field">
                                    <label>Address</label>
                                    <input type="text" name="address" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12 field">
                                    <label>Contact No</label>
                                    <input type="text" name="contact" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12 field">
                                    <label>E-mail</label>
                                    <input type="email" name="email" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12 field">
                                    <label>Religion</label>
                                    <select class="form-control" name="religion">
                                        <option value="Islam">Islam</option>
                                        <option value="Hinduism">Hinduism</option>
                                        <option value="Buddhist">Buddhist</option>
                                        <option value="Christians">Christians</option>
                                        <option value="Others">Others</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-12 field">
                                    <label>Photograph (Passport size)</label>
                                    <input type="file" name="picture" class="form-control" accept="image/*" required>
                                </div>
                            </div>
                            <br><br>
                            <div class="form-group row">
                                <div class="col-md-12 field">
                                    <h4>Additional Registration (Only for husband/wife/son/daughter)</h4>
                                    <label for="email">Husband/Wife's Name</label>
                                    <input type="text" name="h_w_name" id="email" class="form-control">
                                </div>
                            </div>
                            <h4>Registration only for above 5 years child. (0-5 years baby free)</h4>
                            <div class="form-group row" id="input_fields_wrap">
                                <div class="col-md-7 field">
                                    <label>Son/Daughter's Name</label>
                                    <input type="text" name="child_name[]" class="form-control">
                                </div>
                                <div class="col-md-3 field">
                                    <label>Age</label>
                                    <input type="number" name="child_age[]" class="form-control" min="6">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12 field">
                                    <button id="add_field_button" class="btn btn-info">Add</button>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12 field">
                                    <label class="checkbox-inline"><input type="checkbox" required><h4>Make sure all of these information are correct.</h4></label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12 field">
                                    <input type="submit" id="submit" class="btn btn-primary" value="Submit">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
        
    </div>
</div>
    <?php include ("../templateLayout/footer.php");?>
</div>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
</div>
<?php include ("../templateLayout/script/templateScript.php");?>
</body>
</html>