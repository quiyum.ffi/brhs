<?php
require_once ("../../vendor/autoload.php");
session_start();
include ("../templateLayout/templateInformation.php");
use App\Authentication;
use App\Utility\Utility;
if($_SESSION['role_status']==0){
    $auth= new Authentication();
    $status = $auth->setData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('../user/login.php');
        return;
    }
}
else {
    Utility::redirect('../user/login.php');
}
use App\user\Registration;
$information= new Registration();
$allData=$information->showall();
$oneData=$information->showallreq();
?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title;?></title>
    <?php include ("../templateLayout/css/templateCss.php");?>
    <?php include('../templateLayout/css/tableCss.php');?>
</head>
<body>
<div class="fh5co-loader"></div>
<div id="page">
    <?php include ("../templateLayout/adminNavigation.php");?>
    <div id="fh5co-blog-popular">
        <div class="container">
            <div class="row animate-box">
                <div class="col-md-12 col-md-offset-0 text-center fh5co-heading">
                    <h2><span>Member Request</span></h2>
                </div>
            </div>
            <h2 style="text-align:center;">Total Member Request: <b><?php echo $oneData->totalrow?><b></h2>
            <?php
            use App\Message\Message;
            if(isset($_SESSION) && !empty($_SESSION['message'])) {

                $msg = Message::getMessage();

                echo "
                        <p id='message' style='text-align: center; font-family: Pristina; font-size: 25px'>$msg</p>";

            }
            ?>
            <div class="row">
                <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th style='text-align: center;width: 7%'>Serial</th>
                            <th style='text-align: center'>Reference ID</th>
                            <th style='text-align: center'>Name</th>
                            <th style='text-align: center'>Address</th>
                            <th style='text-align: center'>Contact</th>
                            <th style='text-align: center'>Batch</th>
                            <th style='text-align: center'>Request Date</th>
                            <th style='text-align: center'>Amount</th>
                            <th style='text-align: center'>Status</th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $serial= 1;
                        foreach($allData as $oneData){
                            $date=date('d-m-Y h:i:s a', strtotime($oneData->registration_date));
                            echo" <tr>
                                    <td style='text-align: center'>$serial</td>
                                    <td style='text-align: center'>$oneData->id</td>
                                    <td style='text-align: center'>$oneData->user_name</td>
                                    <td style='text-align: center'>$oneData->address</td>
                                    <td style='text-align: center'>$oneData->contact</td>
                                    <td style='text-align: center'>$oneData->passing_year</td>
                                    <td style='text-align: center'>$date</td>
                                    <td style='text-align: center'>$oneData->total_amount</td>
                                    <td style='text-align: center'>
                                          <a href='memberProfile.php?reference_id=$oneData->id' class='btn btn-primary'>View</a>
                                    </td>
                                </tr>";
                            $serial++;

                        }?>

                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>

    <?php include ("../templateLayout/footer.php");?>
</div>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
</div>
<?php include ("../templateLayout/script/templateScript.php");?>
<?php include('../templateLayout/script/tableScript.php');?>
</body>
</html>


