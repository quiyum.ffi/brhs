<?php include ("../templateLayout/templateInformation.php");?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title;?></title>
    <?php include ("../templateLayout/css/templateCss.php");?>
</head>
<body>
<div id="page">
    <?php include ("../templateLayout/navigation.php");?>
    <h2 style="text-align: center"><img src="../../resources/photos/logo.png" width="100px">Bangladesh Railway Govt. High School, Saltgola, Chittagong</h2>
     <h3 style="text-align: center">Re-union: <b style="color:green">22th December,2017</b></h3>
    <h3 style="text-align: center">Last date of registration: <b style="color:red">30th November,2017</b></h3>
    <aside id="fh5co-hero">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="flexslider animate-box">
                        <ul class="slides">
                            <li style="background-image: url(../../resources/images/1.jpg);">
                                <div class="overlay-gradient"></div>
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1 slider-text">
                                            <div class="slider-text-inner">
                                                <h1>Please Registration for Reunion- 2017</h1>
                                                <h2>Bangladesh Railway Govt High School, Saltgola, Chittagong</h2>
                                                <p class="ct"><a href="registration-step1.php">Online Registration<i class="icon-arrow-right"></i></a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li style="background-image: url(../../resources/images/2.jpg);">
                                <div class="overlay-gradient"></div>
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1 slider-text">
                                            <div class="slider-text-inner">
                                                <h1>Please Registration for Reunion- 2017</h1>
                                                <h2>Bangladesh Railway Govt High School, Saltgola, Chittagong</h2>
                                                <p class="ct"><a href="registration-step1.php">Online Registration<i class="icon-arrow-right"></i></a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li style="background-image: url(../../resources/images/img_bg_3.jpg);">
                                <div class="overlay-gradient"></div>
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1 slider-text">
                                            <div class="slider-text-inner">
                                                <h1>Please Registration for Reunion- 2017</h1>
                                                <h2>Bangladesh Railway Govt High School, Saltgola, Chittagong</h2>
                                                <p class="ct"><a href="registration-step1.php">Online Registration<i class="icon-arrow-right"></i></a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-8 col-sm-8">
                    <hr>
                    <h2>Image Gallery</h2>
                    <hr>
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="thumbnail">
                                <img src="<?php echo base_url?>resources/images/gallery/1.jpg" style="width:100%">
                                <div class="caption">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="thumbnail">
                                <img src="<?php echo base_url?>resources/images/gallery/2.jpg" style="width:100%">
                                <div class="caption">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="thumbnail">
                                <img src="<?php echo base_url?>resources/images/gallery/3.jpg" style="width:100%">
                                <div class="caption">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="thumbnail">
                                <img src="<?php echo base_url?>resources/images/gallery/4.jpg" style="width:100%">
                                <div class="caption">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="thumbnail">
                                <img src="<?php echo base_url?>resources/images/gallery/5.jpg" style="width:100%">
                                <div class="caption">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="thumbnail">
                                <img src="<?php echo base_url?>resources/images/gallery/6.jpg" style="width:100%">
                                <div class="caption">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div style="padding-top: 20px;">
                        <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fbangladeshrailwayhighschool&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="100%" height="550" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                    </div>
                    <h4>Visit our facebook page</h4>
                    <a href="https://www.facebook.com/bangladeshrailwayhighschool/" target="_blank"><img src="../../resources/photos/logo.png" class="img-responsive"></a>
                </div>
            </div>
        </div>
    </aside>
    <?php include ("../templateLayout/footer.php");?>
</div>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
</div>
    <?php include ("../templateLayout/script/templateScript.php");?>
</body>
</html>


