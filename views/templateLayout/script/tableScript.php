
<!-- DataTables JavaScript -->
<script src="<?php echo base_url; ?>resources/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url; ?>resources/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>