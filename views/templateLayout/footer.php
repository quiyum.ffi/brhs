
<footer id="fh5co-footer" role="contentinfo">
    <div class="container">
        <div class="row row-pb-md">
            <div class="col-md-4 fh5co-widget">
                <h4>About Event</h4>
                <p>Reunion of Bangladesh Railway Govt High School, Saltgula, Chittagong</p>
            </div>
            <div class="col-md-4 col-md-push-1">
                <h4>Links</h4>
                <ul class="fh5co-footer-links">
                    <li><a href="index.php">Home</a></li>
                    <li><a href="schedule.php">Schedule</a></li>
                    <li><a href="contact.php">Contact</a></li>
                </ul>
            </div>

            <div class="col-md-4 col-md-push-1">
                <h4>Contact Information</h4>
                <ul class="fh5co-footer-links">
                    <li>Reunion Committee, <br> Bangladesh Railway Govt High School, Saltgula, Chittagong</li>
                    <li><a href="#">+ 880 1829 820421</a></li>
                    <li><a href="#">faruk9747@gmail.com</a></li>
                    <li><a href="#">shahadat.ni7@gmail.com</a></li>
                </ul>
            </div>

        </div>

        <div class="row copyright">
            <div class="col-md-12 text-center">
                <p>
                    <small class="block">&copy; 2017 Future Features of IT. All Rights Reserved.</small>
                    <small class="block">Designed by <a href="http://ffibd.com/" target="_blank">F.F.I.</a> Innovation & Excellence In Customized Software And Web Solution.</small>
                </p>
                <p>
                <ul class="fh5co-social-icons">
                    <li><a href="#"><i class="icon-twitter"></i></a></li>
                    <li><a href="#"><i class="icon-facebook"></i></a></li>
                    <li><a href="#"><i class="icon-linkedin"></i></a></li>
                    <li><a href="#"><i class="icon-dribbble"></i></a></li>
                </ul>
                </p>
            </div>
        </div>

    </div>
</footer>
