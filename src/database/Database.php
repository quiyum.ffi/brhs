<?php

namespace App\database;
use PDO, PDOException;

class Database
{
    public $DBH;
    public function __construct()
    {
        try{

            $this->DBH = new PDO('mysql:host=localhost;dbname=ffibd_brhs', "ffibd_brhs", "brhs123$");
            $this->DBH->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );

            return $this->DBH;
        }
        catch(PDOException $error){
            echo "Database Error: ". $error->getMessage();
        }
    }
}