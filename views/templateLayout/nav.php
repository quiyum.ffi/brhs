


<?php
$base_url=base_url;
$index=$base_url.'views/user/index.php';
$schedule=$base_url.'views/user/schedule.php';
$guest=$base_url.'views/user/guest.php';
$contact=$base_url.'views/user/contact.php';
$reg=$base_url.'views/user/registration-step1.php';
$reg2=$base_url.'views/user/registration-step2.php';
$login=$base_url.'views/user/login.php';
$url ='http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
if($url==$index){
    $indexClass="active";
}
elseif($url==$schedule){
    $scheduleClass="active";
}
elseif($url==$guest){
    $guestClass="active";
}
elseif($url==$contact){
    $contactClass="active";
}
elseif($url==$reg){
    $regClass="active";
}
elseif($url==$reg2){
    $regClass="active";
}
elseif($url==$login){
    $logClass="active";
}
?>
<nav class="fh5co-nav" role="navigation">
    <div class="container-fluid">
        <div class="row">
            <div class="top-menu">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-7 text-left menu-1">
                            <ul>
                                <li class="<?php echo $indexClass?>"><a href="<?php echo base_url;?>views/user/index.php">Home</a></li>
                                <li class="<?php echo $scheduleClass?>"><a href="<?php echo base_url;?>views/user/schedule.php">Schedule</a></li>
                                <li class="<?php echo $guestClass?>"><a href="<?php echo base_url;?>views/user/guest.php">Guests</a></li>
                                <li class="<?php echo $contactClass?>"><a href="<?php echo base_url;?>views/user/contact.php">Contact</a></li>
                                <li class="<?php echo $regClass?>"><a href="<?php echo base_url;?>views/user/registration-step1.php">Online Registration</a></li>
                                <li class="<?php echo $logClass?>"><a href="<?php echo base_url;?>views/user/login.php">Admin Login</a></li>

                            </ul>
                        </div>
                        <div class="col-sm-5">
                            <ul class="fh5co-social-icons">
                                <li><a href="#"><i class="icon-twitter-with-circle"></i></a></li>
                                <li><a href="#"><i class="icon-facebook-with-circle"></i></a></li>
                                <li><a href="#"><i class="icon-linkedin-with-circle"></i></a></li>
                                <li><a href="#"><i class="icon-dribbble-with-circle"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 text-center menu-2">
                <div id="fh5co-logo">
                    <h1>
                        <a href="<?php echo base_url;?>views/user/index.php">
                            Reunion<span>.</span>
                            <small>B.R.H.S.</small>
                        </a>
                    </h1>
                </div>
            </div>
        </div>
    </div>
</nav>
