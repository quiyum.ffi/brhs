<?php
require_once ("../../vendor/autoload.php");
session_start();
include ("../templateLayout/templateInformation.php");

?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title;?></title>
    <?php include ("../templateLayout/css/templateCss.php");?>
</head>
<body>
<div class="fh5co-loader"></div>
<div id="page">
    <?php include ("../templateLayout/navigation.php");?>

    <?php
    use App\Message\Message;
    if(isset($_SESSION) && !empty($_SESSION['message'])) {

        $msg = Message::getMessage();

        echo "
                        <p id='message' style='text-align: center; font-family: Pristina; font-size: 25px'>$msg</p>";

    }
    ?>

    <div id="fh5co-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-padded-right">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="fh5co-blog animate-box">
                                <div class="title title-pin text-center">
                                    <span class="posted-on">Alert!</span>
                                    <h3><a href="#">Page is under construction</a></h3>
                                    <span class="category">Keep touching with us</span>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <?php include ("../templateLayout/footer.php");?>
</div>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
</div>
<?php include ("../templateLayout/script/templateScript.php");?>
</body>
</html>


